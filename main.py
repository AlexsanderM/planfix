import gspread as gspread
from oauth2client.service_account import ServiceAccountCredentials


if __name__ == '__main__':
    # Google
    gscope = ["https://spreadsheets.google.com/feeds", 'https://www.googleapis.com/auth/spreadsheets',
              "https://www.googleapis.com/auth/drive.file", "https://www.googleapis.com/auth/drive"]
    gcredentials = "planfix-322616-b06095bdfce7.json"
    gdocument = "planfix"

    credentials = ServiceAccountCredentials.from_json_keyfile_name(gcredentials, gscope)

    gc = gspread.authorize(credentials)
    wks = gc.open(gdocument).sheet1
    print(wks.get('A1'))